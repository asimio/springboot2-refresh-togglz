# README #

Refreshing Togglz Feature Flags using Spring Cloud Config Server source code for blog post at https://tech.asimio.net/2020/11/12/Refreshing-Feature-Flags-using-Togglz-and-Spring-Cloud-Config-Server.html

### Requirements ###

* Java 8
* Maven 3.2.x
* Spring Cloud Config Server
    * https://tech.asimio.net/2016/12/09/Centralized-and-Versioned-Configuration-using-Spring-Cloud-Config-Server-and-Git.html
    * https://bitbucket.org/asimio/configserver

### Building the artifact ###

```
mvn clean package
```

### Running the application from command line ###

```
mvn spring-boot:run
```

### Available URLs

```
curl "http://localhost:8080/api/demo-someservice"
```
should result in successful responses.

### Switch toggles

- https://bitbucket.org/asimio/demo-config-properties/src/master/springboot2-refresh-togglz.yml
- Update `togglz.features.USE_NEW_SOMESERVICE.enabled` and force reloading properties in client application via:

```
curl -v -H "Content-Type: application/json" localhost:8080/actuator/refresh -d {}
```
- Send request again to Available URLs.

### Who do I talk to? ###

* orlando.otero at asimio dot net
* https://www.linkedin.com/in/ootero
