package com.asimio.demo.togglz.config;

import org.togglz.core.Feature;
import org.togglz.core.annotation.Label;

public enum FeatureToggles implements Feature {

    @Label("New some service.")
    USE_NEW_SOMESERVICE;
}