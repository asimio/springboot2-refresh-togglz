package com.asimio.demo.togglz.config;

import java.io.IOException;
import java.util.Map;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cloud.context.config.annotation.RefreshScope;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.context.annotation.Primary;
import org.springframework.core.io.Resource;
import org.springframework.core.io.ResourceLoader;
import org.togglz.core.repository.StateRepository;
import org.togglz.core.repository.cache.CachingStateRepository;
import org.togglz.core.repository.file.FileBasedStateRepository;
import org.togglz.core.repository.mem.InMemoryStateRepository;
import org.togglz.spring.boot.actuate.autoconfigure.TogglzProperties;
import org.togglz.spring.boot.actuate.autoconfigure.TogglzProperties.FeatureSpec;
import org.togglz.spring.proxy.FeatureProxyFactoryBean;

import com.asimio.demo.togglz.service.SomeService;
import com.asimio.demo.togglz.service.impl.NewSomeServiceImpl;
import com.asimio.demo.togglz.service.impl.OldSomeServiceImpl;

@Configuration
public class AppConfig {

    @Autowired
    private TogglzProperties togglzProperties;

    @Autowired
    private ResourceLoader resourceLoader;

    @Bean
    public SomeService oldSomeService() {
        return new OldSomeServiceImpl();
    }

    @Bean
    public SomeService newSomeService() {
        return new NewSomeServiceImpl();
    }

    @Bean
    public FeatureProxyFactoryBean proxiedSomeService() {
        FeatureProxyFactoryBean proxyFactoryBean = new FeatureProxyFactoryBean();
        proxyFactoryBean.setFeature(FeatureToggles.USE_NEW_SOMESERVICE.name());
        proxyFactoryBean.setProxyType(SomeService.class);
        proxyFactoryBean.setActive(this.newSomeService());
        proxyFactoryBean.setInactive(this.oldSomeService());
        return proxyFactoryBean;
    }

    @Bean
    @RefreshScope
    public StateRepository stateRepository() throws IOException {
        StateRepository stateRepository;
        String featuresFile = this.togglzProperties.getFeaturesFile();
        if (featuresFile != null) {
            Resource resource = this.resourceLoader.getResource(featuresFile);
            Integer minCheckInterval = this.togglzProperties.getFeaturesFileMinCheckInterval();
            if (minCheckInterval != null) {
                stateRepository = new FileBasedStateRepository(resource.getFile(), minCheckInterval);
            } else {
                stateRepository = new FileBasedStateRepository(resource.getFile());
            }
        } else {
            Map<String, FeatureSpec> features = this.togglzProperties.getFeatures();
            stateRepository = new InMemoryStateRepository();
            for (String name : features.keySet()) {
                stateRepository.setFeatureState(features.get(name).state(name));
            }
        }
        // If caching is enabled wrap state repository in caching state repository.
        if (this.togglzProperties.getCache().isEnabled()) {
            stateRepository = new CachingStateRepository(
                    stateRepository,
                    this.togglzProperties.getCache().getTimeToLive(),
                    this.togglzProperties.getCache().getTimeUnit()
            );
        }
        return stateRepository;
    }

    @Bean
    @Primary
    public SomeService someService(@Autowired FeatureProxyFactoryBean proxiedSomeService) throws Exception {
        return (SomeService) proxiedSomeService.getObject();
    }
}