package com.asimio.demo.togglz.web;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.asimio.demo.togglz.service.SomeService;

@RestController
@RequestMapping(value = "/api")
public class DemoController {

    @Autowired
    private SomeService someService;

    @GetMapping(value = "/demo-someservice")
    public String getDemoSomeService() {
        return this.someService.getSomeValue();
    }
}